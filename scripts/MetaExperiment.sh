#! /bin/bash

. blissrc

BASE_FOLDER=\${BLISSADM}/applications/${python.package.name}

APPLICATION=${python.package.name}/MetaExperiment.py

cd \${BASE_FOLDER}

exec python \${APPLICATION} "$@"

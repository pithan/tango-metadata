#! /bin/bash

. blissrc

BASE_FOLDER=\${BLISSADM}/applications/${python.package.name}

APPLICATION=${python.package.name}/HDF5Export.py

cd \${BASE_FOLDER}

exec python \${APPLICATION} -w https://ovm-icat.esrf.fr:8181/ICATService/ICAT?wsdl --no-proxy .esrf.fr,localhost -a esrf -u $1 -P -o data/$3 $2 